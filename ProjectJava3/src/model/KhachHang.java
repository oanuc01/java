/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author 84966
 */
public class KhachHang {
    int id;
    String tenkhachhang,diachikhachhang,sodienthoai;

    public KhachHang(int id, String tenkhachhang, String diachikhachhang, String sodienthoai) {
        this.id = id;
        this.tenkhachhang = tenkhachhang;
        this.diachikhachhang = diachikhachhang;
        this.sodienthoai = sodienthoai;
    }

    public KhachHang() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenkhachhang() {
        return tenkhachhang;
    }

    public void setTenkhachhang(String tenkhachhang) {
        this.tenkhachhang = tenkhachhang;
    }

    public String getDiachikhachhang() {
        return diachikhachhang;
    }

    public void setDiachikhachhang(String diachikhachhang) {
        this.diachikhachhang = diachikhachhang;
    }

    public String getSodienthoai() {
        return sodienthoai;
    }

    public void setSodienthoai(String sodienthoai) {
        this.sodienthoai = sodienthoai;
    }
    
}
