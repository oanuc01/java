/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Connect;
//package stackjava.com.demomysqljdbc.demo;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author HP
 */
public class ConnectHelper {
    static Connection connection = null;
    static String url = "jdbc:mysql://localhost:3306/itplus";
    static String user = "root";
    static String password = "";

    public static Connection getConnection() {
       
        try {
//             Class.forName("something.jdbc.driver.YourFubarDriver");  
            connection = DriverManager.getConnection(url, user, password);
        } catch (java.sql.SQLException ex) {
            ex.printStackTrace();
            System.out.println("Loi");
        }
        return connection;
    }
}
