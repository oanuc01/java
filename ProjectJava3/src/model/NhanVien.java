/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author HP
 */
public class NhanVien {
    int id;
    String tenNV,diachi,sodienthoai;
    int namsinh,idchuvu;
    String gioitinh;

    public NhanVien(int id, String tenNV, String diachi, String sodienthoai, int namsinh, int idchuvu, String gioitinh) {
        this.id = id;
        this.tenNV = tenNV;
        this.diachi = diachi;
        this.sodienthoai = sodienthoai;
        this.namsinh = namsinh;
        this.idchuvu = idchuvu;
        this.gioitinh = gioitinh;
    }

    public NhanVien() {
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenNV() {
        return tenNV;
    }

    public void setTenNV(String tenNV) {
        this.tenNV = tenNV;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getSodienthoai() {
        return sodienthoai;
    }

    public void setSodienthoai(String sodienthoai) {
        this.sodienthoai = sodienthoai;
    }

    public int getNamsinh() {
        return namsinh;
    }

    public void setNamsinh(int namsinh) {
        this.namsinh = namsinh;
    }

    public int getIdchuvu() {
        return idchuvu;
    }

    public void setIdchuvu(int idchuvu) {
        this.idchuvu = idchuvu;
    }

    public String getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(String gioitinh) {
        this.gioitinh = gioitinh;
    }

    
    
}