/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.KhachHang;

/**
 *
 * @author 84966
 */
public class KhachHangDao extends AbstractFunction {

    static String query;
    KhachHang kh = new KhachHang();

    public boolean insert(KhachHang kh) throws SQLException {
        query = "insert into khach_hang"
                + "(ten_khach_hang,dia_chi_khach_hang,so_dien_thoai) "
                + "values(?,?,?) ";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, kh.getTenkhachhang());
        pstmt.setString(2, kh.getDiachikhachhang());
        pstmt.setString(3, kh.getSodienthoai());
        return pstmt.execute();

    }

    public List<KhachHang> show() throws SQLException {
        ArrayList<KhachHang> KhachHangList = new ArrayList<>();
        query = "SELECT * FROM khach_hang";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            KhachHang kh1 = new KhachHang(
                    rs.getInt("id"),
                    rs.getString("ten_khach_hang"),
                    rs.getString("dia_chi_khach_hang"),
                    rs.getString("so_dien_thoai")
            );
            KhachHangList.add(kh1);
        }
        return KhachHangList;
    }

    public boolean update(KhachHang kh) throws SQLException {
        query = "UPDATE khach_hang"
                + "SET  ten_khach_hang = ? ,dia_chi_khach_hang = ?, so_dien_thoai =  ? "
                + "WHERE id = '" + kh.getId() + "'";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);

        pstmt.setString(1, kh.getTenkhachhang());
        pstmt.setString(2, kh.getDiachikhachhang());
        pstmt.setString(3, kh.getSodienthoai());
//        System.out.println(kh.getSodienthoai());
        return pstmt.execute();

    }

    public boolean delete(int i) throws SQLException {
        query = "DELETE FROM khach_hang WHERE id = '" + i + "'";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);

        return pstmt.execute();
    }
}
