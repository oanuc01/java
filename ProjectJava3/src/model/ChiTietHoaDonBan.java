/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author 84966
 */
public class ChiTietHoaDonBan {
    int id,idSP,soluongban,dongiaban,thanhtien;
    int idKH;
    int idNV;
    String ngayban;
    int tongtien;

    public ChiTietHoaDonBan(int id, int idSP, int soluongban, int dongiaban, int thanhtien) {
        this.id = id;
        this.idSP = idSP;
        this.soluongban = soluongban;
        this.dongiaban = dongiaban;
        this.thanhtien = thanhtien;
    }

    public ChiTietHoaDonBan() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdSP() {
        return idSP;
    }

    public void setIdSP(int idSP) {
        this.idSP = idSP;
    }

    public int getSoluongban() {
        return soluongban;
    }

    public void setSoluongban(int soluongban) {
        this.soluongban = soluongban;
    }

    public int getDongiaban() {
        return dongiaban;
    }

    public void setDongiaban(int dongiaban) {
        this.dongiaban = dongiaban;
    }

    public int getThanhtien() {
        return thanhtien;
    }

    public void setThanhtien(int thanhtien) {
        this.thanhtien = thanhtien;
    }

    public int getIdKH() {
        return idKH;
    }

    public void setIdKH(int idKH) {
        this.idKH = idKH;
    }

    public int getIdNV() {
        return idNV;
    }

    public void setIdNV(int idNV) {
        this.idNV = idNV;
    }

    public String getNgayban() {
        return ngayban;
    }

    public void setNgayban(String ngayban) {
        this.ngayban = ngayban;
    }

    public int getTongtien() {
        return tongtien;
    }

    public void setTongtien(int tongtien) {
        this.tongtien = tongtien;
    }
    
    
    
    
    
}
