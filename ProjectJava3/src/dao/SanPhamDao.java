/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;
//import 

import Connect.ConnectHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.SanPham;
import javax.swing.JOptionPane;


/**
 *
 * @author HP
 */
 public class SanPhamDao extends AbstractFunction {
    static String query;
    SanPham sp = new SanPham();
    public boolean insert(SanPham sp) throws SQLException {
        query = "insert into san_pham"
                + "(ten_san_pham,don_vi_tinh,ngay_san_xuat,han_su_dung) "
                + "values(?,?,?,?) ";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1,sp.getTenSP());
        pstmt.setString(2,sp.getDVT());
        pstmt.setString(3,sp.getNSX());
        pstmt.setString(4,sp.getHSD());
       
        return pstmt.execute();

    }
    public boolean update(SanPham sp) throws SQLException{
        query = "UPDATE san_pham " +
        "SET  ten_san_pham = ? ,don_vi_tinh = ?, ngay_san_xuat = ? ,han_su_dung = ?" +
        "WHERE id = '" + sp.getId()+"'";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);

        pstmt.setString(1,sp.getTenSP());
        pstmt.setString(2,sp.getDVT());
        pstmt.setString(3,sp.getNSX());
        pstmt.setString(4,sp.getHSD());
       
        return pstmt.execute();
    
    }
    
        
        public boolean delete(int i) throws SQLException {
            
             query = "DELETE FROM san_pham WHERE id = '" + i +"'";
             Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
//            System.out.println("ban ghi" +sp.getMaSP() + sp.getTenSP() 
//                    +sp.getdon_vi_tinh() + sp.getngay_san_xuat() + sp.gethan() +sp.getGia());
//             
            return pstmt.execute();
        }


    public List<SanPham> show() throws SQLException {
        ArrayList<SanPham> sanPhamList = new ArrayList<>();
                      query = "SELECT * FROM san_pham";
                      Connection connection = ConnectHelper.getConnection();
                      Statement stmt = connection.createStatement();
                      ResultSet rs = stmt.executeQuery(query);
                      while(rs.next()){
                          SanPham nv = new SanPham(
                                  rs.getString("ten_san_pham"),
                                  rs.getString("don_vi_tinh"),
                                  rs.getString("ngay_san_xuat"),
                                  rs.getString("han_su_dung"),
                                  rs.getInt("id")
                          );
                          sanPhamList.add(nv);
                      }
                      return sanPhamList;  
    
    }
//    public List<SanPham> orderByDESC() throws SQLException {
//        ArrayList<SanPham> sanPhamList = new ArrayList<>();
//                      query = "SELECT * FROM san_pham\n" +
//                        "ORDER BY han_su_dung DESC;";
//                      Connection connection = ConnectHelper.getConnection();
//                      Statement stmt = connection.createStatement();
//                      ResultSet rs = stmt.executeQuery(query);
//                      while(rs.next()){
//                          SanPham nv = new SanPham(
//                                  rs.getString("ten_san_pham"),
//                                  rs.getString("don_vi_tinh"),
//                                  rs.getString("ngay_san_xuat"),
//                                  rs.getString("han_su_dung"),
//                                  rs.getInt("id")
//                          );
//                          sanPhamList.add(nv);
//                      }
//                      return sanPhamList;  
//    
//    }
//    public List<SanPham> orderByASC() throws SQLException {
//        ArrayList<SanPham> sanPhamList = new ArrayList<>();
//                      query = "SELECT * FROM sp\n" +
//                        "ORDER BY gia ASC;";
//                      Connection connection = ConnectHelper.getConnection();
//                      Statement stmt = connection.createStatement();
//                      ResultSet rs = stmt.executeQuery(query);
//                      while(rs.next()){
//                          SanPham nv = new SanPham(
//                                  rs.getString("ten_san_pham"),
//                                  rs.getString("don_vi_tinh"),
//                                  rs.getString("ngay_san_xuat"),
//                                  rs.getString("han_su_dung"),
//                                  rs.getInt("id")
//                          );
//                          sanPhamList.add(nv);
//                      }
//                      return sanPhamList;  
//    
//    }
    
}
