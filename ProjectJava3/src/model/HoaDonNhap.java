/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author HP
 */
public class HoaDonNhap {
    String maHDN,maNV,maNCC;
    int tongtien;
    String ngaynhap;

    public HoaDonNhap(String maHDN, String maNV, String maNCC, int tongtien, String ngaynhap) {
        this.maHDN = maHDN;
        this.maNV = maNV;
        this.maNCC = maNCC;
        this.tongtien = tongtien;
        this.ngaynhap = ngaynhap;
    }

    public HoaDonNhap() {
    }

    public String getMaHDN() {
        return maHDN;
    }

    public void setMaHDN(String maHDN) {
        this.maHDN = maHDN;
    }

    public String getMaNV() {
        return maNV;
    }

    public void setMaNV(String maNV) {
        this.maNV = maNV;
    }

    public String getMaNCC() {
        return maNCC;
    }

    public void setMaNCC(String maNCC) {
        this.maNCC = maNCC;
    }

    public int getTongtien() {
        return tongtien;
    }

    public void setTongtien(int tongtien) {
        this.tongtien = tongtien;
    }

    public String getNgaynhap() {
        return ngaynhap;
    }

    public void setNgaynhap(String ngaynhap) {
        this.ngaynhap = ngaynhap;
    }
    
}
