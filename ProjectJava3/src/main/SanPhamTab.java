/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package main;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.SanPham;
import dao.SanPhamDao;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author HP
 */
public class SanPhamTab extends javax.swing.JFrame {

    SanPham sp = new SanPham();
    SanPhamDao spd = new SanPhamDao();

    DefaultTableModel model;

    public SanPhamTab() {
        initComponents();
        model = (DefaultTableModel) jTable1.getModel();
        try {
            resultShow();
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamTab.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void FillOut() {

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        HSD = new com.toedter.calendar.JDateChooser();
        NSX = new com.toedter.calendar.JDateChooser();
        tenSP = new javax.swing.JTextField();
        DVT = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        thaydoi = new javax.swing.JLabel();
        fillNone = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setForeground(new java.awt.Color(255, 102, 51));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel1.setText("SAN PHAM");

        jLabel3.setText("TenSP");

        jLabel5.setText("Don vi tinh");

        jLabel6.setText("NSX");

        jLabel7.setText("HSD");

        HSD.setDateFormatString("YYYY-MM-dd");

        NSX.setDateFormatString("YYYY-MM-dd");

        jButton1.setText("Tao ban ghi");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Sua ban ghi");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Xoa ban ghi");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "STT", "Ten san pham", "don vi tinh", "NSX", "HSD"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        fillNone.setText("Lam rong");
        fillNone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fillNoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fillNone, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(thaydoi)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(tenSP, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(DVT, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(HSD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(NSX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(97, 97, 97))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(250, 250, 250)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(thaydoi)
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(DVT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5)
                        .addComponent(tenSP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(HSD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(NSX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(fillNone))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(31, 31, 31))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

//THEM
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        SimpleDateFormat fm = new SimpleDateFormat("YYYY-MM-dd");
        Date nsx = NSX.getDate();
        String strNSX = fm.format(nsx);
        Date hsd = HSD.getDate();
        String strHsd = fm.format(hsd);

        sp.setTenSP(tenSP.getText());
        sp.setDVT(DVT.getText());
        sp.setNSX(strNSX);
        sp.setHSD(strHsd);

//        sp.setGioitinh();
        SanPhamDao spd = new SanPhamDao();
        try {
            spd.insert(sp);
            resultShow();
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamTab.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

//CLICK
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

        int index = jTable1.getSelectedRow();
        SimpleDateFormat fm = new SimpleDateFormat("YYYY-MM-dd");
        int id1 = Integer.parseInt(model.getValueAt(index, 0).toString());
        String tenSP1 = model.getValueAt(index, 1).toString();
        String DVT1 = model.getValueAt(index, 2).toString();
        String HSD1 = model.getValueAt(index, 4).toString();
        String NSX1 = model.getValueAt(index, 3).toString();
        Date hsd = null;
        Date nsx = null;
        try {
            hsd = fm.parse(HSD1);
            nsx = fm.parse(NSX1);
        } catch (ParseException ex) {
            Logger.getLogger(SanPhamTab.class.getName()).log(Level.SEVERE, null, ex);
        }
        tenSP.setText(tenSP1);
        DVT.setText(DVT1);
        HSD.setDate(hsd);
        NSX.setDate(nsx);
        System.out.println(id1 + "---- " + tenSP1 + "---- " + DVT1 + "---- " + HSD1 + "---- " + NSX1);
    }//GEN-LAST:event_jTable1MouseClicked

//SUA
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        SimpleDateFormat fm = new SimpleDateFormat("YYYY-MM-dd");
        Date nsx = NSX.getDate();
        String strNSX = fm.format(nsx);
        Date hsd = HSD.getDate();
        String strHsd = fm.format(hsd);
        int index = jTable1.getSelectedRow();
        sp.setId(Integer.parseInt(model.getValueAt(index, 0).toString()));
        sp.setTenSP(tenSP.getText());
        sp.setDVT(DVT.getText());
        sp.setNSX(strNSX);
        sp.setHSD(strHsd);
        try {
            spd.update(sp);
            resultShow();
            fillNone();
            JOptionPane.showInputDialog("Da thay doi thanh cong");
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamTab.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButton2ActionPerformed

//XOA
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        SimpleDateFormat fm = new SimpleDateFormat("YYYY-MM-dd");
        Date nsx = NSX.getDate();
        String strNSX = fm.format(nsx);
        Date hsd = HSD.getDate();
        String strHsd = fm.format(hsd);
//        System.out.println(maSP.getText());
        int index = jTable1.getSelectedRow();
        sp.setId(Integer.parseInt(model.getValueAt(index, 0).toString()));
        sp.setTenSP(tenSP.getText());
        sp.setDVT(DVT.getText());
        sp.setNSX(strNSX);
        sp.setHSD(strHsd);

        try {
            spd.delete(sp.getId());
            resultShow();
            fillNone();
//           JOptionPane.showInputDialog("Da thay doi thanh cong");
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamTab.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButton3ActionPerformed


    private void fillNoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fillNoneActionPerformed
        // TODO add your handling code here:
        this.fillNone();
    }//GEN-LAST:event_fillNoneActionPerformed
//
//public void orderByDescShow() throws SQLException{ 
//      List<SanPham> lists = spd.orderByDESC();
//        model.setRowCount(0);
//         lists.forEach((list)->{
//                   model.addRow(new Object[]{
//                      list.getId(),
//                       list.getTenSP(),
//                       list.getDVT(),
//                       list.getNSX(),
//                       list.getHSD(),
//
//                   });
//               });
//}
//
//public void orderByAscShow() throws SQLException{ 
//      List<SanPham> lists = spd.orderByASC();
//        model.setRowCount(0);
//         lists.forEach((list)->{
//                   model.addRow(new Object[]{
//                      list.getId(),
//                       list.getTenSP(),
//                       list.getDVT(),
//                       list.getNSX(),
//                       list.getHSD(),
//
//                   });
//               });
//}

//   HIEN THI
    public void resultShow() throws SQLException {
//        SanPhamDao spd = new SanPhamDao();
//        if (orderBy.)

        List<SanPham> lists = spd.show();
        model.setRowCount(0);
        lists.forEach((list) -> {
            model.addRow(new Object[]{
                list.getId(),
                list.getTenSP(),
                list.getDVT(),
                list.getNSX(),
                list.getHSD(),});
        });
    }

//    Fill None
    public void fillNone() {
        tenSP.setText("");
        DVT.setText("");
        HSD.setDate(null);
        NSX.setDate(null);
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SanPhamTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SanPhamTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SanPhamTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SanPhamTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SanPhamTab().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField DVT;
    private com.toedter.calendar.JDateChooser HSD;
    private com.toedter.calendar.JDateChooser NSX;
    private javax.swing.JButton fillNone;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField tenSP;
    private javax.swing.JLabel thaydoi;
    // End of variables declaration//GEN-END:variables
}
