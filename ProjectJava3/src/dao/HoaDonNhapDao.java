/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Connect.ConnectHelper;
//import static dao.HoaDonBanDao.query;
//import static dao.SanPhamDao.query;
//import static dao.HoaDonBanDao.query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.HoaDonBan;
import model.HoaDonNhap;


/**
 *
 * @author HP
 */
public class HoaDonNhapDao extends  AbstractFunction{
    public static String query;
    
    
      public ArrayList<String> showMaNV() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT maNV FROM nv";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("maNV"));

        }
//        System.out.println("");
        return items;
    }
       public ArrayList<String> showMaNCC() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT maNCC FROM ncc";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("maNCC"));

        }
//        System.out.println("");
        return items;
    }
    
      
      
      
     public boolean insert(HoaDonNhap hdn) throws SQLException {
        query = "insert into hdn"
                + "(maHDN,maNV,maNCC,tongtien,ngaynhap) "
                + "values(?,?,?,?,?) ";
//        System.out.println(hdn.getMaHDB());
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, hdn.getMaHDN());
        pstmt.setString(2, hdn.getMaNV());
        pstmt.setString(3, hdn.getMaNCC());
        pstmt.setInt(4, hdn.getTongtien());
        pstmt.setString(5, hdn.getNgaynhap());
        

        return pstmt.execute();
    }
    
    public boolean update(HoaDonNhap hdn) throws SQLException {
        query = "UPDATE hdn " +
                "SET  maNV = ?,maNCC = ?,tongtien = ?,ngaynhap = ? " +
                "WHERE maHDN = '" + hdn.getMaHDN()+"'";
                Connection connection = ConnectHelper.getConnection();
                PreparedStatement pstmt = connection.prepareStatement(query);
        //        pstmt.setString(1,hdn.getManhanvien() );
                pstmt.setString(1,hdn.getMaNV());
                pstmt.setString(2,hdn.getMaNCC());
                pstmt.setInt(3,hdn.getTongtien());
                pstmt.setString(4,hdn.getNgaynhap());
//                pstmt.setInt(5,hdn.getGia());

                return pstmt.execute();    }

    @Override
    public boolean delete(String i) throws SQLException {

             query = "DELETE FROM hdn WHERE maHDN = '" + i +"'";
             Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            return pstmt.execute();  
    }

   
    public List<HoaDonNhap> show(HoaDonNhap hdn) throws SQLException {
            ArrayList<HoaDonNhap> hoadonnhapList = new ArrayList<>();
                                 query = "SELECT * FROM hdn";
                                 Connection connection = ConnectHelper.getConnection();
                                 Statement stmt = connection.createStatement();
                                 ResultSet rs = stmt.executeQuery(query);
                                 while(rs.next()){
                                     HoaDonNhap hdn1 = new HoaDonNhap(
                                             rs.getString("maHDN"),
                                             rs.getString("maNV"),
                                             rs.getString("maNCC"),
                                             rs.getInt("tongtien"),
                                             rs.getString("ngaynhap")
                                     );
                                     hoadonnhapList.add(hdn1);
                                 }
                                 return hoadonnhapList;  
    }

    @Override
    public List<Object> orderBy(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
