/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Connect.ConnectHelper;
import static dao.ChiTietKhoDao.query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.HoaDonBan;

/**
 *
 * @author HP
 */
public class HoaDonBanDao extends AbstractFunction {
    
    HoaDonBan hdb = new HoaDonBan();
//    HoaDonBanDao hdbd = new HoaDonBanDao();
    public static String query;
//SHOW ITEM

    public ArrayList<String> showItem() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT maSP FROM sp";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("maSP"));

        }
//        System.out.println("");
        return items;
    }

//Show ma NV    
    public ArrayList<String> showMaNV() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT maNV FROM nv";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("maNV"));

        }
//        System.out.println("");
        return items;
    }

//    public ArrayList<String> showMaKH() throws SQLException {
//        ArrayList<String> items = new ArrayList<>();
//        query = "SELECT maKH FROM kh";
//        Connection connection = ConnectHelper.getConnection();
//        Statement stmt = connection.createStatement();
//        ResultSet rs = stmt.executeQuery(query);
//        while (rs.next()) {
//            items.add(rs.getString("maKH"));
//
//        }
////        System.out.println("");
//        return items;
//    }


//INSERT
    public boolean insert(HoaDonBan hdb) throws SQLException {
        query = "insert into hdb"
                + "(maNV,Vat,tongtien,ngayban,maSP,soluong) "
                + "values(?,?,?,?,?,?) ";
//        System.out.println(hdb.getMaHDB());
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
//        pstmt.setString(1, hdb.getMaHDB());
//        pstmt.setString(2, hdb.getMaKH());
        pstmt.setString(1, hdb.getMaNV());
        pstmt.setInt(2, hdb.getVAT());
        pstmt.setInt(3, hdb.getTongtien());
        pstmt.setString(4, hdb.getNgayban());
        pstmt.setString(5, hdb.getMaSP());
        pstmt.setInt(6, hdb.getSoluong());

        return pstmt.execute();
    }

//   UPDATE
    @Override
    public boolean update(Object ob) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

//    DELETE
    @Override
    public boolean delete(String sp) throws SQLException {
            
             query = "DELETE FROM hdb WHERE maSP = '" + sp +"'";
             Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            return pstmt.execute();
        }
//SHOW

    public List<HoaDonBan> show() throws SQLException {
        ArrayList<HoaDonBan> HoaDonBanList = new ArrayList<>();
        query = "SELECT * FROM hdb";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            HoaDonBan hdb = new HoaDonBan(
                    rs.getString("maNV"),
                    rs.getInt("Vat"),
                    rs.getInt("tongtien"),
                    rs.getString("ngayban"),
                    rs.getInt("soluong"),
                    rs.getString("maSP") 
            );
            HoaDonBanList.add(hdb);
        }
        return HoaDonBanList;
    }
//ORDERBY

    @Override
    public List<Object> orderBy(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

//    LAYSP
    public int showGiaTien(HoaDonBan hdb) throws SQLException {
//        String items ;
        query = "SELECT gia FROM sp WHERE maSP ='" + hdb.getMaSP() + "'";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            int gia = rs.getInt("gia");

            return gia;

        }
        return 0;

//        System.out.println("");
//        return gia;
    }
    public ArrayList<Integer> showPrice() throws SQLException {
        ArrayList<Integer> Prices = new ArrayList<>();
        query = "SELECT tongtien FROM hdb";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            Prices.add(rs.getInt("tongtien"));

        }
//        System.out.println("");
        return Prices;
    }
    

}
