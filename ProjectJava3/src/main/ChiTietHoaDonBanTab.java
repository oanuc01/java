/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package main;

import dao.ChiTietHoaDonBanDao;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.ChiTietHoaDonBan;

/**
 *
 * @author mac
 */
public class ChiTietHoaDonBanTab extends javax.swing.JFrame {
    ChiTietHoaDonBan cthdb = new ChiTietHoaDonBan();
    ChiTietHoaDonBanDao cthdbd = new ChiTietHoaDonBanDao();
    DefaultTableModel model;

    public ChiTietHoaDonBanTab() throws SQLException {
        initComponents();
        tenNV.setText("Nguyen Phi Long");
        model = (DefaultTableModel) jTable1.getModel();
        currentDate.setText(getCurrentDate());
         randomText.setText(randomHDB());
        showComboBox();
        resultShow();

    }
//SHOW TOTAL
    public int total() throws SQLException {
        ArrayList<Integer> prices = cthdbd.showPrice();
        int total = 0;
        for (int i = 0; i < prices.size(); i++) {
            total += prices.get(i);

        }
        return total;
    }

//      SHOW RESULT
    public void resultShow() throws SQLException {
        List<ChiTietHoaDonBan> lists = cthdbd.show();
        model.setRowCount(0);
        lists.forEach((list) -> {
            model.addRow(new Object[]{
                list.getId(),
                list.getIdSP(),
                list.getSoluongban(),
                list.getDongiaban(),
                list.getThanhtien()
            });
        });
        String tongHoaDonHienThi = String.valueOf(total());
        tonghoadon.setText(tongHoaDonHienThi);
    }

//    SHOW COMBOBOX
    public void showComboBox() throws SQLException {
//SHOW ITEM
        ArrayList<String> items = cthdbd.showItem();
        items.forEach((item) -> tenSP.addItem(item));
////Show MA kH
        ArrayList<String> itemsKH = cthdbd.showTenKH();
        itemsKH.forEach((itemKH) -> tenKH.addItem(itemKH));
    }

//    GET CURRENT DATE
    public String getCurrentDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

//RANDOM HDB
    public String randomHDB() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

//    System.out.println(generatedString);
        return generatedString;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        PRINT = new javax.swing.JButton();
        RETURN = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        ADD = new javax.swing.JButton();
        DELETE = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        randomText = new javax.swing.JLabel();
        currentDate = new javax.swing.JLabel();
        tenSP = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        tonghoadon = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        soluongbanSP = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        dongiaban = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        tenKH = new javax.swing.JComboBox<>();
        tenNV = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        toggleBTN = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Chi tiet hoa don ban");

        jLabel2.setText("Mã HDB");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "id", "id san pham", "so luong ban", "don gia ban", "thanh tien"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        PRINT.setText("Xuất HD");
        PRINT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PRINTActionPerformed(evt);
            }
        });

        RETURN.setText("Quay lại");

        jLabel8.setText("TONG HOA DON");

        ADD.setText("Tao");
        ADD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ADDActionPerformed(evt);
            }
        });

        DELETE.setText("Xoa");
        DELETE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DELETEActionPerformed(evt);
            }
        });

        jLabel4.setText("Ngay ban");

        randomText.setText("jLabel10");

        currentDate.setText("jLabel10");

        tenSP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tenSPMouseClicked(evt);
            }
        });
        tenSP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tenSPActionPerformed(evt);
            }
        });

        jLabel10.setText("ten san pham");

        tonghoadon.setText("jLabel11");

        jLabel12.setText("So lương sp");

        jLabel13.setText("Don gia ban");

        jLabel14.setText("ten khach hang");

        tenKH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tenKHMouseClicked(evt);
            }
        });
        tenKH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tenKHActionPerformed(evt);
            }
        });

        tenNV.setText("Nguyen Phi Long");

        jLabel16.setText("Nhan vien:");

        toggleBTN.setText("Chon KH");
        toggleBTN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toggleBTNActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(PRINT)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(RETURN)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ADD)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(DELETE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(tonghoadon))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 533, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(randomText)
                                .addGap(196, 196, 196)
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(currentDate))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(74, 74, 74)
                                .addComponent(soluongbanSP, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel14)
                                            .addComponent(jLabel13))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(dongiaban, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tenKH, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(toggleBTN)))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tenNV))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel10))
                                .addGap(12, 12, 12)
                                .addComponent(tenSP, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(145, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(tenNV))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(tenSP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(dongiaban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(soluongbanSP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(tenKH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(toggleBTN)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PRINT)
                    .addComponent(RETURN)
                    .addComponent(ADD)
                    .addComponent(DELETE)
                    .addComponent(jLabel8)
                    .addComponent(tonghoadon))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(randomText)
                    .addComponent(currentDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
//PRINT
    private void PRINTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PRINTActionPerformed
        
    }//GEN-LAST:event_PRINTActionPerformed
//ADD
    private void ADDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ADDActionPerformed
       if(toggleBTN.isSelected()){
            String tenSPCBB = (String) tenSP.getSelectedItem();
             cthdb.setSoluongban(Integer.parseInt(soluongbanSP.getText()));
             cthdb.setDongiaban(Integer.parseInt(dongiaban.getText()));
             cthdb.setThanhtien((cthdb.getDongiaban() * cthdb.getSoluongban()));
            try { 
                cthdb.setIdSP(cthdbd.priceSwitch(tenSPCBB));
                cthdbd.insert(cthdb);
                resultShow();
            } catch (SQLException ex) {
                Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{

            JOptionPane.showInputDialog("Chi nhan duy nhat 1 khach hang");
        }
    }//GEN-LAST:event_ADDActionPerformed
//CLICK
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
         int index =jTable1.getSelectedRow();
        int idctb1 = Integer.parseInt( model.getValueAt(index,0).toString());
        int idSP1 = Integer.parseInt( model.getValueAt(index,1).toString());
        int soluongban1 = Integer.parseInt( model.getValueAt(index,2).toString());
        int dongiaban1 = Integer.parseInt( model.getValueAt(index,3).toString());
        int thanhtien =Integer.parseInt( model.getValueAt(index,4).toString());
        try {
            tenSP.setSelectedItem(cthdbd.ConvertTenID(idSP1));
            soluongbanSP.setText(String.valueOf(soluongban1));
        } catch (SQLException ex) {
            Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(Level.SEVERE, null, ex);
        }
        cthdb.setId(idctb1);
    }//GEN-LAST:event_jTable1MouseClicked
//DELETE
    private void DELETEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DELETEActionPerformed
        // TODO add your handling code here:
        System.out.println(cthdb.getId());
        try {
            cthdbd.delete(cthdb.getId());
            resultShow();
            soluongbanSP.setText("");
        } catch (SQLException ex) {
            Logger.getLogger(SanPhamTab.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_DELETEActionPerformed

    private void tenSPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tenSPMouseClicked

    }//GEN-LAST:event_tenSPMouseClicked
//CHON SAN PHAM 
    private void tenSPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tenSPActionPerformed
        // TODO add your handling code here:
        String giaban1 = null ;
         String tenSanPham = (String) tenSP.getSelectedItem();
//        System.out.println(tenSanPham);
        try {
            int idPrice = cthdbd.priceSwitch(tenSanPham);
             giaban1 =  String.valueOf(cthdbd.priceDisplay(idPrice)) ;
        } catch (SQLException ex) {
            Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(Level.SEVERE, null, ex);
        }
        dongiaban.setText(giaban1);
        dongiaban.setEnabled(false);
        
    }//GEN-LAST:event_tenSPActionPerformed

    private void tenKHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tenKHActionPerformed

    }//GEN-LAST:event_tenKHActionPerformed

    private void tenKHMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tenKHMouseClicked


    }//GEN-LAST:event_tenKHMouseClicked
//TOGLE ACTION
    private void toggleBTNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleBTNActionPerformed
        // TODO add your handling code here:
        if(toggleBTN.isSelected()){
            tenKH.setEnabled(false);
        }else{
            tenKH.setEnabled(true);
        }
    }//GEN-LAST:event_toggleBTNActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new ChiTietHoaDonBanTab().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(ChiTietHoaDonBanTab.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ADD;
    private javax.swing.JButton DELETE;
    private javax.swing.JButton PRINT;
    private javax.swing.JButton RETURN;
    private javax.swing.JLabel currentDate;
    private javax.swing.JTextField dongiaban;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel randomText;
    private javax.swing.JTextField soluongbanSP;
    private javax.swing.JComboBox<String> tenKH;
    private javax.swing.JLabel tenNV;
    private javax.swing.JComboBox<String> tenSP;
    private javax.swing.JToggleButton toggleBTN;
    private javax.swing.JLabel tonghoadon;
    // End of variables declaration//GEN-END:variables
}
