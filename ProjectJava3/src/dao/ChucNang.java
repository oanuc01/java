/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author HP
 */ 
  interface  ChucNang {
      boolean insert(Object ob) throws SQLException;
       List<Object>show(Object ob) throws SQLException;
       boolean update(Object ob) throws SQLException; 
       boolean delete(String i) throws SQLException; 
//       boolean orderBy(Object ob) throws SQLException;
       List<Object>orderBy(Object ob) throws SQLException;


}
