/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Connect.ConnectHelper;
import static dao.SanPhamDao.query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ChiTietHoaDonBan;

/**
 *
 * @author 84966
 */
public class ChiTietHoaDonBanDao extends  AbstractFunction {
    ChiTietHoaDonBan cthdb = new ChiTietHoaDonBan();
        public static String query;
//SHOW ITEM TEN SAN PHAM
    public ArrayList<String> showItem() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT ten_san_pham FROM san_pham";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("ten_san_pham"));

        }
        return items;
    }
// SHOW TEN KHACH HANG   
      public ArrayList<String> showTenKH() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT ten_khach_hang FROM khach_hang";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("ten_khach_hang"));

        }
        return items;
    }
    
//    Lay ID san pham 
     public int priceSwitch(String tenSanPham) throws SQLException {
         int item ;
        query = "SELECT id FROM san_pham WHERE ten_san_pham = '"+tenSanPham+"'";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
              item = rs.getInt("id");
              return item;
        }
        
        return 0;
    }
//     HIEN THI GIA CHINH THUC thong qua ID SAN PHAM
     public int priceDisplay(int idPrice) throws SQLException {
         int item ;
         query = "SELECT gia_ban FROM chi_tiet_kho WHERE id_san_pham = '"+idPrice+"'";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
              item = rs.getInt("gia_ban");
              System.out.println(item);
              return item;
        }
        
        return 0;
     
     }

    
    
    public boolean insert(ChiTietHoaDonBan cthdb) throws SQLException {
        query = "insert into chi_tiet_hoa_don_ban"
                + "(id_san_pham,so_luong_ban,don_gia_ban,thanh_tien) "
                + "values(?,?,?,?) ";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1,cthdb.getIdSP());
        pstmt.setInt(2,cthdb.getSoluongban());
        pstmt.setInt(3,cthdb.getDongiaban());
        pstmt.setInt(4,cthdb.getThanhtien());
       
        return pstmt.execute();
    }

    public List<ChiTietHoaDonBan> show() throws SQLException {
         ArrayList<ChiTietHoaDonBan> ChiTietHoaDonBanList = new ArrayList<>();
        query = "SELECT * FROM chi_tiet_hoa_don_ban";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            ChiTietHoaDonBan hdb = new ChiTietHoaDonBan(
                    rs.getInt("id"),
                    rs.getInt("id_san_pham"),
                    rs.getInt("so_luong_ban"),
                    rs.getInt("don_gia_ban"),
                    rs.getInt("thanh_tien")
            );
            ChiTietHoaDonBanList.add(hdb);
        }
        return ChiTietHoaDonBanList;
    }
    
    public ArrayList<Integer> showPrice() throws SQLException {
        ArrayList<Integer> Prices = new ArrayList<>();
        query = "SELECT thanh_tien FROM chi_tiet_hoa_don_ban";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            Prices.add(rs.getInt("thanh_tien"));

        }
//        System.out.println("");
        return Prices;
    }
        public String ConvertTenID(int sp ) throws SQLException{
             String error = "error";
            query = "SELECT  ten_san_pham  from  san_pham where id= '"+sp+"'";
            Connection connection = ConnectHelper.getConnection();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next())
                {
                   String  a =  rs.getString("ten_san_pham");
//                   System.out.println("ID cua san pham nay la" + a);
                      return a;
                }
                
                return error;
        }
    
    
    public boolean update(ChiTietHoaDonBan cthdb) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


    public boolean delete(int i) throws SQLException {

         query = "DELETE  FROM chi_tiet_hoa_don_ban WHERE id = '" + i +"'";
             Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            return pstmt.execute();
    }


    public List<Object> orderBy(Object ob) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    

   
    
}
