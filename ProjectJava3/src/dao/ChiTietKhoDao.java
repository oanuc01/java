/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Connect.ConnectHelper;
import static dao.SanPhamDao.query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ChiTietKho;
import model.SanPham;

/**
 *
 * @author HP
 */
public class ChiTietKhoDao extends AbstractFunction {

    ChiTietKho ctk = new ChiTietKho();
    public static String query;

    public ArrayList<String> showItem() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT ten_san_pham FROM san_pham";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("ten_san_pham"));

        }
//        System.out.println("");
        return items;

    }

    public ArrayList<String> showDistinicItem() throws SQLException {
        ArrayList<String> items = new ArrayList<>();
        query = "SELECT DISTINCT maKho\n"
                + "\n"
                + "FROM kho";
        Connection connection = ConnectHelper.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            items.add(rs.getString("maKho"));

        }
        return items;

    }

    public boolean insert(ChiTietKho ctk) throws SQLException {
        query = "insert into chi_tiet_kho"
                + "(id_san_pham,so_luong_san_pham,gia_ban) "
                + "values(?,?,?) ";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(2, ctk.getSoluongSP());
        pstmt.setInt(1, ctk.getIdSP());
        pstmt.setInt(3, ctk.getGiaban());
        return pstmt.execute();
    }

//    show
    public List<ChiTietKho> show() throws SQLException {
        ArrayList<ChiTietKho> ChiTietKhoList = new ArrayList<>();
                      query = "SELECT * FROM chi_tiet_kho";
                      Connection connection = ConnectHelper.getConnection();
                      Statement stmt = connection.createStatement();
                      ResultSet rs = stmt.executeQuery(query);
                      while(rs.next()){
                          ChiTietKho ctk = new ChiTietKho(
                                  rs.getInt("id"),
                                  rs.getInt("id_san_pham"),
                                  rs.getInt("so_luong_san_pham"),
                                  rs.getInt("gia_ban")
                          );
                          ChiTietKhoList.add(ctk);
                      }
                      return ChiTietKhoList; 
    }
//    Show search
     public List<ChiTietKho> showSearch(String search) throws SQLException {
       
        ArrayList<ChiTietKho> ChiTietKhoList = new ArrayList<>();
                      query = "SELECT * FROM chi_tiet_kho "
                              +" WHERE id_san_pham = '"+ this.ConvertTenSP(search)+"'";
                      Connection connection = ConnectHelper.getConnection();
                      Statement stmt = connection.createStatement();
                      ResultSet rs = stmt.executeQuery(query);
                      while(rs.next()){
                          ChiTietKho ctk = new ChiTietKho(
                                  rs.getInt("id"),
                                  rs.getInt("id_san_pham"),
                                  rs.getInt("so_luong_san_pham"),
                                  rs.getInt("gia_ban")
                          );
                          ChiTietKhoList.add(ctk);
                      }
                      return ChiTietKhoList; 
    }
    
//UPDATE `chi_tiet_kho` SET `so_luong_san_pham`=60,`gia_ban`= 8000 WHERE `id`='2';
    public boolean update(ChiTietKho ctk) throws SQLException {
        query = "UPDATE chi_tiet_kho "
                + "SET so_luong_san_pham = ?,gia_ban = ?"
                + "WHERE id = ? ";
        Connection connection = ConnectHelper.getConnection();
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1, ctk.getSoluongSP());
        pstmt.setInt(2, ctk.getGiaban());
        pstmt.setInt(3, ctk.getId());
        System.out.println(ctk.getId());
        return pstmt.execute();
    }

    public boolean delete(int sp) throws SQLException {
            
             query = "DELETE FROM chi_tiet_kho WHERE id = '" + sp +"'";
             Connection connection = ConnectHelper.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            return pstmt.execute();
        }

    
        public int ConvertTenSP(String sp ) throws SQLException{
            query = "SELECT  id  from  san_pham where ten_san_pham = '"+sp+"'";
            Connection connection = ConnectHelper.getConnection();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next())
                {
                   int  a =  rs.getInt("id");
//                   System.out.println("ID cua san pham nay la" + a);
                      return a;
                }
                
                return 0;
        }
    
         public String ConvertTenID(int sp ) throws SQLException{
             String error = "error";
            query = "SELECT  ten_san_pham  from  san_pham where id= '"+sp+"'";
            Connection connection = ConnectHelper.getConnection();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next())
                {
                   String  a =  rs.getString("ten_san_pham");
                   System.out.println("ID cua san pham nay la" + a);
                      return a;
                }
                
                return error;
        }
    
//    @Override
//    public boolean orderBy(Object ob) throws SQLException {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }

    public List<ChiTietKho> showSearch() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
